/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.ox_program.Table;
/**
 *
 * @author Administrator
 */
public class TableUnitTest {
    
    public TableUnitTest() {
    }

    @org.junit.jupiter.api.BeforeAll
    public static void setUpClass() throws Exception {
    }

    @org.junit.jupiter.api.AfterAll
    public static void tearDownClass() throws Exception {
    }

    @org.junit.jupiter.api.BeforeEach
    public void setUp() throws Exception {
    }

    @org.junit.jupiter.api.AfterEach
    public void tearDown() throws Exception {
    }
    
    @BeforeAll
    public static void setUpClass() {
        
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
   
    public void testRow1Wim(){
        Player o = new Player('o');
        Player x = new Player('x');  
        Table table = new Table(o,x);
        
        table.setRowCol(1,1);
        table.setRowCol(1,2);
        table.setRowCol(1,3);
       assertEquals(true,table.checkWin());
    }
    public void testCol1Wim(){
        Player o = new Player('o');
        Player x = new Player('x');  
        Table table = new Table(o,x);
        
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
       assertEquals(true,table.checkWin());
    }
    
        public void testSwitchPlayer(){
        Player o = new Player('o');
        Player x = new Player('x');  
        Table table = new Table(o,x);
        table.switchPlayer();
  
       assertEquals(true,table.getCurrentPlayer().getName());
    }
        
         public void testX1Wim(){
        Player o = new Player('o');
        Player x = new Player('x');  
        Table table = new Table(o,x);
        
        table.setRowCol(1,3);
        table.setRowCol(2,2);
        table.setRowCol(3,1);
        assertEquals(true,table.checkWin());
    }
         public void testX2Wim(){
        Player o = new Player('o');
        Player x = new Player('x');  
        Table table = new Table(o,x);
        
        table.setRowCol(1,1);
        table.setRowCol(2,1);
        table.setRowCol(3,1);
       assertEquals(true,table.checkWin());
    }
     // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
