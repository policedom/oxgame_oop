/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class Table {
    
    char[][] detail ={{'_','_','_'},{'_','_','_'},{'_','_','_'}};
    Player P1;
    Player P2;
    private Player currentPlayer;
    int lastCol;
    int lastRow;

    public Table(Player P1, Player P2) {
        this.P1 = P1;
        this.P2 = P2;
        this.currentPlayer = this.P1;
    }

    public char[][] getDetail() {
        return detail;
    }
    
    public Player getCurrentPlayer(){
        return currentPlayer;
    }
    
    public void switchPlayer(){
        if(this.currentPlayer == P1){
            this.currentPlayer = P2;
        }else{
            this.currentPlayer = P1;
        }
        
    }

    public boolean setRowCol(int row, int col) {
       if(this.detail[row-1][col-1]!= '_'){
           return false;
       }
       this.detail[row-1][col-1] = currentPlayer.getName();
       lastCol = col-1;
       lastRow = row-1;
       return true;
    } 
    public boolean checkRow(){
        for(int col=0; col<this.detail[lastRow].length;col++){
            if(this.detail[lastRow][col] != currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    
        public boolean checkCol(){
        for(int row=0; row<this.detail.length;row++){
            if(this.detail[row][lastCol] != currentPlayer.getName()){
                return false;
            }
        }
        return true;
    }
    public boolean checkX1(){
        for(int i=0; i<this.detail.length;i++){
         if(this.detail[i][i]!=currentPlayer.getName()){
             return false;
         }   
        }
        return true;
    }
    
        public boolean checkX2(){
        for(int i=0; i<this.detail.length;i++){
         if(this.detail[i][this.detail.length-i-1]!=currentPlayer.getName()){
             return false;
         }   
        }
        return true;
    }
    
    public boolean checkWin() {
        if(checkCol() || checkRow()||checkX1() || checkX2()){
             return true;
        }
       return false;
    }
   
}
