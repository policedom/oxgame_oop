
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Administrator
 */
public class Game {
   private Scanner kb = new Scanner(System.in);
    private int col;
    private int row;
    Table table = null;
    Player o = null;
    Player x = null;
    
    public Game(){
       this.o = new Player('o');
       this.x = new Player('x');
    }
    
     public void run(){
         this.showWelcome();
         this.table = new Table(o,x);
         while(true){      
         this.showTable();
         this.showTurn();
         this.inputRowCol();
        
         if(table.checkWin()){
             return;
         }
          table.switchPlayer();
         }
       
     }
     private void showWelcome(){
         System.out.println("Welcome to OX Game");
     }
     
      private void showTable(){
      char[][]detail = this.table.getDetail();
        for(int row=0; row<detail.length;row++){
            System.out.print("|");
            for(int col=0; col<detail[row].length; col++){
                System.out.print(detail[row][col]+" |");
            }
            System.out.println("");
        }
     }
      
      private void showTurn(){
         System.out.println(table.getCurrentPlayer().getName()+ "Turn");
     }
      
       private void input(){
        System.out.print("Please input row col:");
         row = kb.nextInt();
         col = kb.nextInt();
     }

    private void inputRowCol() {
        while(true){
            this.input();
            if(table.setRowCol(row,col)){
             return;
           }
        }
        
    }
}
